# Generate your GraphQL client code

## Requirements

Download `get-graphql-schema` from https://github.com/prismagraphql/get-graphql-schema.

```
npm install -g get-graphql-schema
```

## Download schema from various providers


##### Github

```
get-graphql-schema  --header 'Authorization=Bearer <OAUTH_TOKEN>' https://api.github.com/graphql
``` 

##### Shopify

```
get-graphql-schema --header 'X-Shopify-Storefront-Access-Token=<ACCESS_TOKEN>' https://graphql.myshopify.com/api/graphql
```

##### FluentCommerce

```
get-graphql-schema  --header 'Authorization=Bearer <OAUTH_TOKEN>' --header 'fluent.account=<ACCOUNT>' https://jds.api.fluentretail.com/graphql
```


## License

Apache 2.0
Copyright 2018 Meta3 