package artemis.client.extension;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fluentcommerce.graphql.client.module.Address;
import com.fluentcommerce.graphql.client.module.CustomerAddress;
import com.fluentcommerce.graphql.client.module.StoreAddress;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Slf4j
public class AddressDeserializer extends StdDeserializer<Address> {

    // 2018-07-11T04:15:47.700Z
    public final SimpleDateFormat sdf;

    public AddressDeserializer() {
        this(null);

    }

    public AddressDeserializer(Class<?> vc) {
        super(vc);
        sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public Address deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String type = getNodeText(node, "type");

        try {
            String id = getNodeText(node, "id");
            String ref = getNodeText(node, "ref");
            String name = getNodeText(node, "name");
            String street = getNodeText(node, "street");
            String city = getNodeText(node, "city");
            String state = getNodeText(node, "state");
            String country = getNodeText(node, "country");
            String postcode = getNodeText(node, "postcode");
            String region = getNodeText(node, "region");
            String companyName = getNodeText(node, "companyName");
            String timeZone = getNodeText(node, "timeZone");
            Float longitude = getNodeFloatValue(node, "longitude");
            Float latitude = getNodeFloatValue(node, "latitude");
            Date createdOn = sdf.parse(getNodeText(node, "createdOn"));
            Date updatedOn = sdf.parse(getNodeText(node, "updatedOn"));

            if (type.equals("AGENT")) {
                return StoreAddress.builder()
                        .id(id)
                        .ref(ref)
                        .name(name)
                        .state(street)
                        .city(city)
                        .state(state)
                        .country(country)
                        .postcode(postcode)
                        .region(region)
                        .companyName(companyName)
                        .timeZone(timeZone)
                        .longitude(longitude)
                        .latitude(latitude)
                        .createdOn(createdOn)
                        .updatedOn(updatedOn).build();

            } else if (type.equals("CUSTOMER")) {
                return CustomerAddress.builder()
                        .id(id)
                        .ref(ref)
                        .name(name)
                        .state(street)
                        .city(city)
                        .state(state)
                        .country(country)
                        .postcode(postcode)
                        .region(region)
                        .companyName(companyName)
                        .timeZone(timeZone)
                        .longitude(longitude)
                        .latitude(latitude)
                        .createdOn(createdOn)
                        .updatedOn(updatedOn).build();
            }
        } catch (ParseException e) {
            log.error("Failed to pass field!", e);
            throw new IOException(e);
        }

        throw new IllegalArgumentException("Unable to parase Address with type = "+type);
    }

    private String getNodeText(JsonNode node, String fieldName) {
        JsonNode textNode = node.get(fieldName);

        return textNode != null && textNode instanceof TextNode ? textNode.textValue() : null;
    }

    private Float getNodeFloatValue(JsonNode node, String fieldName) {
        JsonNode floatValNode = node.get(fieldName);

        return floatValNode != null && floatValNode instanceof DoubleNode ? floatValNode.floatValue() : null;
    }
}
