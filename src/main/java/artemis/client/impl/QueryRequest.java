package artemis.client.impl;

import lombok.Builder;
import lombok.Value;

/***
 *
 * @author Valentino Chen (valentino.chen.1983@gmail.com)
 *
 */

@Value
@Builder
public class QueryRequest {

    private String query;

    private String variables;

}
