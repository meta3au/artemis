package artemis.client.impl;

import artemis.graphql.Query;
import artemis.graphql.annotation.InputField;
import artemis.graphql.annotation.Operational;
import artemis.utils.ClassUtil;
import artemis.utils.ExtendedScalaInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.schema.idl.ScalarInfo;
import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.*;

/***
 *
 * @author Valentino Chen (valentino.chen.1983@gmail.com)
 *
 */

@Slf4j
@Builder
@Value
public class QueryComposer {

    private ObjectMapper objectMapper;

    private final int nestedLevels;

    public <Q extends Query> QueryRequest composeQueryRequest(Q query) throws IllegalAccessException, JsonProcessingException {

        if (!query.getClass().isAnnotationPresent(Operational.class)) {
            throw new IllegalArgumentException(query.getClass() + " doesn't contains " + Operational.class.getPackage() +  ".Operational annotation");
        }

        Operational operational = query.getClass().getAnnotation(Operational.class);

        List<Field> inputFields = ClassUtil.getAnnotatedDeclaredFields(query.getClass(), InputField.class, true);

        StringBuilder sb = new StringBuilder();
        sb.append("query ").append(query.getClass().getSimpleName());

        // Store all the input arguments as string literal in "variable" field
        Map<String, Object> variables = new HashMap<>();

        StringJoiner inputDefinitions = new StringJoiner(", ", "(",")");
        StringJoiner inputArguments = new StringJoiner(", ", "(",")");

        for (Field inputField : inputFields) {
            InputField fieldMeta = inputField.getAnnotation(InputField.class);

            // build query schema, e.g. query VirtualPositionQuery($ref: String!, $catalogue: VirtualCatalogueKey!) {
            // virtualPosition(ref: $ref, catalogue: $catalogue) {
            inputDefinitions.add(String.format("$%s: %s", inputField.getName(), fieldMeta.typeDefinition()));
            inputArguments.add(String.format("%s: $%s", inputField.getName(), inputField.getName()));

            inputField.setAccessible(true);
            Object value = inputField.get(query);
            if (value != null) {
                variables.put(inputField.getName(), value);
            }

        }

        sb.append(inputDefinitions.toString())
                .append("{ ")
                .append(operational.name())
                .append(inputArguments.toString())
                .append("{ ");

        // construct return type schema
        buildResponseSchema(query.responseType(), sb, 0);

        sb.append("} }");

        String queryStr = sb.toString();
        String variableStr = objectMapper.writeValueAsString(variables);

        return QueryRequest.builder().query(queryStr).variables(variableStr).build();
    }

    private void buildResponseSchema(Class<?> responseType, StringBuilder sb, int currentLevel) {
        if (responseType.isInterface()) {
            buildResponseSchemaFromInterface(responseType, sb, currentLevel);
        } else {
            buildResponseSchemaFromClass(responseType, sb, currentLevel);
        }
    }

    private void buildResponseSchemaFromInterface(Class<?> interfaceType, StringBuilder sb, int currentLevel) {
        List<Method> getMethods = ClassUtil.getAnnotatedDeclaredMethods(interfaceType, artemis.graphql.annotation.Field.class, true);

        getMethods.stream().forEach( m -> {
            artemis.graphql.annotation.Field fieldMeta = m.getAnnotation(artemis.graphql.annotation.Field.class);
            Class<?> fieldType = m.getReturnType();
            if (List.class.isAssignableFrom(fieldType)) {
                log.debug("List type {}", fieldMeta.name());

                try {
                    fieldType = Class.forName(((ParameterizedType) m.getGenericReturnType()).getActualTypeArguments()[0].getTypeName());
                } catch (ClassNotFoundException e) {
                    log.error("Unable to identify the field type for [{}]", fieldMeta.name());
                }
            }
            builderFunc.apply(fieldType, fieldMeta, sb, currentLevel);
        });
    }

    private void buildResponseSchemaFromClass(Class<?> classType, StringBuilder sb, int currentLevel) {
        List<Field> returnedFields = ClassUtil.getAnnotatedDeclaredFields(classType, artemis.graphql.annotation.Field.class, true);

        returnedFields.stream().forEach(m -> {
            artemis.graphql.annotation.Field fieldMeta = m.getAnnotation(artemis.graphql.annotation.Field.class);
            Class<?> fieldType = m.getType();

            if (List.class.isAssignableFrom(fieldType)) {
                try {
                    fieldType = Class.forName(((ParameterizedType) m.getGenericType()).getActualTypeArguments()[0].getTypeName());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            builderFunc.apply(fieldType, fieldMeta, sb, currentLevel );
        });
    }

    @FunctionalInterface
    interface BuildSchema<O, F, S, I> {
        void apply (O originalField, F fieldMeta, S sb, I currentLevl);
    }

    final BuildSchema<Class<?>, artemis.graphql.annotation.Field, StringBuilder, Integer> builderFunc = new BuildSchema<Class<?>, artemis.graphql.annotation.Field, StringBuilder, Integer>() {
        @Override
        public void apply(Class<?> fieldType, artemis.graphql.annotation.Field fieldMeta, StringBuilder sb, Integer currentLevel) {

            // TODO use some customer type resolver
            if (ScalarInfo.isStandardScalar(fieldMeta.typeDefinition()) || fieldMeta.typeDefinition().equals("DateTime") || fieldMeta.typeDefinition().equals("Json")) {
                sb.append(fieldMeta.name()).append(" ");
            } else {
                if (ExtendedScalaInfo.isConnection(fieldType)) {
                    // TODO lazy loading Connection
                    log.debug("Connection type detected: {}", fieldMeta.name());
                }
                // For all custom type, list their type in a Constants class
                else {
                    if (currentLevel < nestedLevels) {
                        log.debug("Non standard type detected: {}", fieldMeta.name());

                        sb.append(fieldMeta.name()).append(" { ");
                        buildResponseSchema(fieldType, sb, currentLevel + 1);
                        sb.append("} ");
                    } else {
                        log.error("Exceeding nested level of depth for field {}", fieldMeta.name());
                    }
                }
            }
        }
    };
}