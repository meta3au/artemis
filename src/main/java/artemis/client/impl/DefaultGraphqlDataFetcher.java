package artemis.client.impl;

import artemis.client.ArtemisClientConfig;
import artemis.client.GraphqlDataFetcher;
import artemis.graphql.Error;
import artemis.graphql.Mutation;
import artemis.graphql.Query;
import artemis.graphql.annotation.Operational;
import artemis.graphql.exception.GraphQLClientException;
import artemis.graphql.exception.QueryException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/***
 *
 * @author Valentino Chen (valentino.chen.1983@gmail.com)
 *
 */
@Slf4j
public class DefaultGraphqlDataFetcher implements GraphqlDataFetcher {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);


    private final OkHttpClient httpClient;

    private final CacheManager cacheManager;

    private final ArtemisClientConfig config;

    private final QueryComposer queryComposer;


    private DefaultGraphqlDataFetcher(@Nonnull OkHttpClient httpClient, CacheManager cacheManager, @Nonnull QueryComposer queryComposer, Map<Class<?>, JsonDeserializer> deserializers, @Nonnull ArtemisClientConfig config) {
        initDeserializers(deserializers);

        this.httpClient = httpClient;
        this.cacheManager = cacheManager;
        this.queryComposer = queryComposer;
        this.config = config;
    }

    private void initDeserializers(Map<Class<?>, JsonDeserializer> deserializers) {
        if (deserializers != null && !deserializers.isEmpty()) {
            SimpleModule module = new SimpleModule();
            deserializers.forEach( (clazz, deserializer) -> {
                module.addDeserializer(clazz, deserializer);
            });

            OBJECT_MAPPER.registerModule(module);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private OkHttpClient httpClient;

        private CacheManager cacheManager;

        private ArtemisClientConfig config;

        private Map<Class<?>, JsonDeserializer> deserializers = new ConcurrentHashMap<>();

        Builder() {
        }

        public Builder httpClient(OkHttpClient httpClient) {
            this.httpClient = httpClient;
            return this;
        }


        public Builder cacheManager(CacheManager cacheManager) {
            this.cacheManager = cacheManager;
            return this;
        }


        public Builder config(@Nonnull ArtemisClientConfig config) {
            this.config = config;
            return this;
        }

        public Builder addDeserializer(Class<?> clazz, JsonDeserializer deserializer) {
            deserializers.put(clazz, deserializer);
            return this;
        }


        public DefaultGraphqlDataFetcher build() {
            if (httpClient == null) {
                httpClient = new OkHttpClient();
            }

            QueryComposer queryComposer = QueryComposer.builder().objectMapper(OBJECT_MAPPER).nestedLevels(config.getMaxNestedLevels()).build();

            return new DefaultGraphqlDataFetcher(httpClient, cacheManager, queryComposer, deserializers, config);
        }

    }


    @Override
    public <T> T performQueryOperation(Query<T> query, Class<T> responseType) {

        QueryRequest _request = null;
        try {
            _request = queryComposer.composeQueryRequest(query);

            RequestBody body = RequestBody.create(
                    MediaType.parse("application/json; charset=utf-8"), OBJECT_MAPPER.writeValueAsString(_request));

            Request request = new Request.Builder().url(config.getGraphQLEndpoint()).post(body).build();

            Call call = httpClient.newCall(request);
            Response response = call.execute();

            return convertResponseToJavaType(response, responseType);
        } catch (IllegalAccessException | IOException | GraphQLClientException e) {
            // If a request object has been created but there is error, throws {@link QueryException}
            if (_request  != null) {
                Operational operational = query.getClass().getAnnotation(Operational.class);
                throw new QueryException(operational.name(), _request.getQuery(), _request.getVariables(), e);
            }
        }
        return null;
    }


    @Override
    public <T> T performMutationOperation(Mutation<T> mutation, Class<T> responseType) {
        return null;
    }


    private <T> T convertResponseToJavaType(Response response, Class<T> classType) {
        try {
            String respBody;
            if (response.body() != null)
                respBody = response.body().string();
            else
                respBody = null;

            if (respBody == null || "".equals(respBody)) {
                return null;
            }

            log.debug("Response json => {}", respBody);

            if (respBody != null && !"".equals(respBody)) {
                JsonNode dictionary = OBJECT_MAPPER.readValue(respBody, JsonNode.class);

                // handle errors
                if (dictionary.get("errors") != null) {
                    String errorsList = dictionary.get("errors").toString();
                    List<Error> errors = OBJECT_MAPPER.readValue(errorsList, OBJECT_MAPPER.getTypeFactory().constructCollectionType(List.class, Error.class));
                    throw new GraphQLClientException(errors);
                }

                // Retrieve result successfully;
                JsonNode responseObj = dictionary.get("data").iterator().next();

                if (responseObj != null) {
                    String dataJsonStr = responseObj.toString();
                    return OBJECT_MAPPER.readValue(dataJsonStr, classType);
                }
            }

            return null;
        } catch (IOException e) {
            log.error("Failed to deserialize response into Java class", e);
        }

        return null;

    }

}
