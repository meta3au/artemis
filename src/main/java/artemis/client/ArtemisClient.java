package artemis.client;

import artemis.client.extension.AddressDeserializer;
import artemis.client.impl.DefaultGraphqlDataFetcher;
import artemis.graphql.Query;
import com.fluentcommerce.graphql.client.module.Address;
import okhttp3.OkHttpClient;

import javax.annotation.Nonnull;

/***
 *
 * @author Valentino Chen (valentino.chen.1983@gmail.com)
 *
 */

public class ArtemisClient {

    private final GraphqlDataFetcher dataFetcher;

    private ArtemisClient(@Nonnull GraphqlDataFetcher dataFetcher) {
        this.dataFetcher = dataFetcher;
    }

    public static ArtemisClient.Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private OkHttpClient httpClient;
        private ArtemisClientConfig config;
        private GraphqlDataFetcher dataFetcher;

        private Builder() {
        }

        public Builder httpClient(@Nonnull  OkHttpClient httpClient) {
            this.httpClient = httpClient;
            return this;
        }

        public Builder config(@Nonnull ArtemisClientConfig config) {
            this.config = config;
            return this;
        }

        public Builder dataFetcher(@Nonnull GraphqlDataFetcher dataFetcher) {
            this.dataFetcher = dataFetcher;
            return this;
        }

        public ArtemisClient build() {

            if (httpClient == null) {
                httpClient = new OkHttpClient();
            }

            if (dataFetcher == null) {
                dataFetcher = DefaultGraphqlDataFetcher.builder().httpClient(httpClient)
                        .config(config)
                        .build();
            }

            return new ArtemisClient(dataFetcher);
        }
    }


    public <T> T query(@Nonnull Query<T> query) {
        return dataFetcher.performQueryOperation(query, query.responseType());
    }

}
