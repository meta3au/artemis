package artemis.client;

import okhttp3.HttpUrl;

import javax.annotation.Nonnull;

/***
 *
 * @author Valentino Chen (valentino.chen.1983@gmail.com)
 *
 */
public class ArtemisClientConfig {

    public static final int DEFAUL_MAX_NESTED_LEVEL = 3;

    private int maxNestedLevels;

    private final HttpUrl graphQLEndpoint;

    public int getMaxNestedLevels() {
        return maxNestedLevels;
    }

    public HttpUrl getGraphQLEndpoint() {
        return graphQLEndpoint;
    }

    private ArtemisClientConfig(@Nonnull HttpUrl graphQLEndpoint, int maxNestedLevels) {
        this.graphQLEndpoint = graphQLEndpoint;
        this.maxNestedLevels = maxNestedLevels;
    }


    public static Builder builder() {
        return new ArtemisClientConfig.Builder();
    }

    public static class Builder {

        private Builder() {}

        private int maxNestedLevels;

        private HttpUrl graphQLEndpoint;

        public Builder maxNestedLevels(int maxNestedLevels) {
            this.maxNestedLevels = maxNestedLevels;
            return this;
        }

        public Builder graphQLEndpoint(@Nonnull String graphQLEndpoint) {
            this.graphQLEndpoint = HttpUrl.parse(graphQLEndpoint);
            return this;
        }

        public ArtemisClientConfig build() {
            return new ArtemisClientConfig(graphQLEndpoint, maxNestedLevels == 0 ? DEFAUL_MAX_NESTED_LEVEL : maxNestedLevels);
        }
    }

}
