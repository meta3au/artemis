package artemis.client;


import artemis.graphql.Mutation;
import artemis.graphql.Query;

/***
 *
 * @author Valentino Chen (valentino.chen.1983@gmail.com)
 *
 */

public interface GraphqlDataFetcher {


    <T> T performQueryOperation(Query<T> query, Class<T> responseType);

    <T> T performMutationOperation(Mutation<T> mutation, Class<T> responseType);



}
