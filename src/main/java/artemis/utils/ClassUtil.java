package artemis.utils;

import com.fluentcommerce.graphql.client.module.Address;
import com.fluentcommerce.graphql.client.module.CustomerAddress;
import com.fluentcommerce.graphql.client.module.Order;

import java.lang.reflect.Field;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Kupal 3kb
 */
public class ClassUtil {

    /**
     * Create new instance of specified class and type
     *
     * @param clazz of instance
     * @param <T> type of object
     * @return new Class instance
     */
    public static <T> T getInstance(Class<T> clazz) {
        T t = null;
        try {
            t = clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return t;
    }

    /**
     * Retrieving fields list of specified class
     * If recursively is true, retrieving fields from all class hierarchy
     *
     * @param clazz where fields are searching
     * @param recursively param
     * @return list of fields
     */
    public static Field[] getDeclaredFields(Class clazz, boolean recursively) {
        List<Field> fields = new LinkedList<Field>();
        Field[] declaredFields = clazz.getDeclaredFields();
        Collections.addAll(fields, declaredFields);

        Class superClass = clazz.getSuperclass();

        if(superClass != null && recursively) {
            Field[] declaredFieldsOfSuper = getDeclaredFields(superClass, recursively);
            if(declaredFieldsOfSuper.length > 0)
                Collections.addAll(fields, declaredFieldsOfSuper);
        }

        return fields.toArray(new Field[fields.size()]);
    }

    public static Method[] getDeclaredMethods(Class clazz, boolean recursively) {
        List<Method> methods = new LinkedList<>();
        Method[] declaredMethods = clazz.getDeclaredMethods();
        Collections.addAll(methods, declaredMethods);

        Class superClass = clazz.getSuperclass();

        if(superClass != null && recursively) {
            Method[] declaredFieldsOfSuper = getDeclaredMethods(superClass, recursively);
            if(declaredFieldsOfSuper.length > 0)
                Collections.addAll(methods, declaredFieldsOfSuper);
        }

        return methods.toArray(new Method[methods.size()]);
    }

    /**
     * Retrieving fields list of specified class and which
     * are annotated by incoming annotation class
     * If recursively is true, retrieving fields from all class hierarchy
     *
     * @param clazz - where fields are searching
     * @param annotationClass - specified annotation class
     * @param recursively param
     * @return list of annotated fields
     */
    public static List<Field> getAnnotatedDeclaredFields(Class clazz,
                                                     Class<? extends Annotation> annotationClass,
                                                     boolean recursively) {
        Field[] allFields = getDeclaredFields(clazz, recursively);
        List<Field> annotatedFields = new LinkedList<Field>();

        for (Field field : allFields) {
            if(field.isAnnotationPresent(annotationClass))
                annotatedFields.add(field);
        }

        return annotatedFields;
//        return annotatedFields.toArray(new Field[annotatedFields.size()]);
    }


    public static List<Method> getAnnotatedDeclaredMethods(Class clazz,
                                                     Class<? extends Annotation> annotationClass,
                                                     boolean recursively) {
        Method[] allMethods = getDeclaredMethods(clazz, recursively);
        List<Method> annotatedMethods = new LinkedList<>();

        for (Method method : allMethods) {
            if(method.isAnnotationPresent(annotationClass))
                annotatedMethods.add(method);
        }

        return annotatedMethods;
//        return annotatedMethods.toArray(new Method[annotatedMethods.size()]);
    }

    public static void main(String[] args) {
//        System.out.println(ClassUtil.getAnnotatedDeclaredFields(Order.class, artemis.graphql.annotation.Field.class, true).length);

        System.out.println(ClassUtil.getAnnotatedDeclaredMethods(Address.class, artemis.graphql.annotation.Field.class, true).size());

//        System.out.println(ClassUtil.getAnnotatedDeclaredFields(CustomerAddress.class, artemis.graphql.annotation.Field.class, true).length);
    }
}
