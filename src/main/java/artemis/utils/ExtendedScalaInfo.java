package artemis.utils;

public class ExtendedScalaInfo {

    public static boolean isConnection(Class clazz) {

        return clazz.getSimpleName().endsWith("Connection");
    }
}
