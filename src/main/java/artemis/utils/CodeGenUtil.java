package artemis.utils;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import java.util.Random;

public class CodeGenUtil {

    public static String camelCase(String str) {
        StringBuilder builder = new StringBuilder(str);
        // Flag to keep track if last visited character is a
        // white space or not
        boolean isLastSpace = true;

        // Iterate String from beginning to end.
        for (int i = 0; i < builder.length(); i++) {
            char ch = builder.charAt(i);

            if (isLastSpace && ch >= 'a' && ch <= 'z') {
                // Character need to be converted to uppercase
                builder.setCharAt(i, (char) (ch + ('A' - 'a')));
                isLastSpace = false;
            } else if (ch != ' ')
                isLastSpace = false;
            else
                isLastSpace = true;
        }

        return builder.toString();
    }

    public static void main(String a[]) {
        /*
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(PersonService.class);
        enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {
            if (method.getDeclaringClass() != Object.class && method.getReturnType() == String.class) {
                return "Hello Tom!";
            } else if (method.getDeclaringClass() != Object.class && method.getReturnType() == Integer.class) {
                return new Random().nextInt();

            //else if (method.getName().equals("lengthOfName")) {
//                return 100;
            } else {
                return proxy.invokeSuper(obj, args);
            }
        });

        PersonService proxy = (PersonService) enhancer.create();
        */

        PersonService proxy = (PersonService) Enhancer.create(PersonService.class,  (MethodInterceptor) (obj, method, args, proxiedClass) -> {
            if (method.getDeclaringClass() != Object.class && method.getReturnType() == String.class) {
                return "Hello Tom!";
            } else if (method.getDeclaringClass() != Object.class && method.getReturnType() == Integer.class) {
                return new Random().nextInt();

            } else {
                return proxiedClass.invokeSuper(obj, args);
            }
        });


        String res = proxy.sayHello(null);

        int length = proxy.lengthOfName("Jeff");
        System.out.println(res);

        System.out.println("length -> "+ length);
    }


}
