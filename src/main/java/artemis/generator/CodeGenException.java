package artemis.generator;



public class CodeGenException extends RuntimeException {

    public CodeGenException(String message) {
        super(message);
    }
}
