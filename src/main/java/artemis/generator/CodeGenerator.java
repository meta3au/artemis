package artemis.generator;

import artemis.graphql.annotation.*;
import artemis.graphql.annotation.Field;
import artemis.utils.CodeGenUtil;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.squareup.javapoet.*;
import graphql.language.*;
import graphql.language.TypeName;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.lang.model.element.Modifier;
import javax.validation.constraints.NotEmpty;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.time.temporal.ChronoUnit.SECONDS;


@Slf4j
public class CodeGenerator {

    public static final String DIRECTIVE_CACHE_KEY = "cacheKey";

    private static final String GEN_SOURCE = "client/src/main/generated";

    public static void main(String arg[]) throws IOException {
        CodeGenerator generator = new CodeGenerator();
        String packageName = "com.fluentcommerce.graphql.client.module";
        String queryPackageName = "com.fluentcommerce.graphql.client.query";
        String mutationPackageName = "com.fluentcommerce.graphql.client.mutation";

        SchemaParser schemaParser = new SchemaParser();
        Map<String, TypeDefinition> typeDefinitionMap = new HashMap<>();
        TypeDefinitionRegistry typeDefinitionRegistry =
                schemaParser.parse(new InputStreamReader(
                        CodeGenerator.class.getClassLoader().getResourceAsStream("fluentcommerce.schema")));
        //
        typeDefinitionRegistry.types().forEach((type, typeDefinition) -> {

            try {
                typeDefinitionMap.put(typeDefinition.getName(), typeDefinition);

                if (typeDefinition instanceof InterfaceTypeDefinition) {
                    generator.generateInterface(packageName, (InterfaceTypeDefinition) typeDefinition);
                } else if (typeDefinition instanceof ObjectTypeDefinition) {
                    generator.generateObject(packageName, (ObjectTypeDefinition) typeDefinition);
                } else if (typeDefinition instanceof InputObjectTypeDefinition) {
                    generator.generateInputObject(packageName, (InputObjectTypeDefinition) typeDefinition);
                } else if (typeDefinition instanceof EnumTypeDefinition) {
                    generator.generateEnum(packageName, (EnumTypeDefinition) typeDefinition);
                } else {
                    log.warn("Unable to identify the type [{}] to process", typeDefinition.getName());
                }

            } catch (Exception e) {
                log.error("Fail to process code generation for {}", type, e);

            }
        });

        TypeDefinition queryType = typeDefinitionRegistry.getType("Query").get();

        queryType.getChildren().forEach(node -> {

            FieldDefinition fieldDefinition = (FieldDefinition) node;

            try {
                String outputType = ((TypeName) fieldDefinition.getType()).getName();
                generator.generateQuery(packageName, queryPackageName, fieldDefinition, typeDefinitionMap.get(outputType));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        TypeDefinition mutationType = typeDefinitionRegistry.getType("Mutation").get();

        mutationType.getChildren().forEach(node -> {
            FieldDefinition fieldDefinition = (FieldDefinition) node;

            try {
                String outputType = ((TypeName) fieldDefinition.getType()).getName();
                generator.generateMutation(packageName, mutationPackageName, fieldDefinition, typeDefinitionMap.get(outputType));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    public void generateQuery(String modulePackageName, String queryPackageName, FieldDefinition field, TypeDefinition output) throws IOException {
        String className = CodeGenUtil.camelCase(field.getName() + "Query");
        TypeSpec.Builder builder = TypeSpec.classBuilder(className)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(getGeneratedSpec())
                .addAnnotation(lombok.Value.class)
                .addAnnotation(AnnotationSpec.builder(Builder.class)
                        .addMember("builderClassName", "$S", "Builder")
                        .addMember("toBuilder", "$L", true).build())
                .addAnnotation(AnnotationSpec.builder(JsonDeserialize.class)
                        .addMember("builder", "$T.class", ClassName.get(queryPackageName, className + ".Builder")).build()
                );


        field.getInputValueDefinitions().forEach(inputValueDefinition -> {
            graphql.language.Type fieldType = inputValueDefinition.getType();
            boolean isList = false;
            boolean isNonNull = false;
            boolean isListNonEmpty = false;

            String inputTypeName = null;

            if (fieldType instanceof NonNullType) {
                NonNullType nonNullType = (NonNullType) fieldType;
                isNonNull = true;
                log.info("{}.{} type = {}, is ListType? {}, isNonnull type? {}", field.getName(), inputValueDefinition.getName(), inputValueDefinition.getType(), nonNullType.getType() instanceof ListType, nonNullType.getType() instanceof TypeName);

                if (nonNullType.getType() instanceof ListType) {
                    isList = true;
                    ListType _listType = (ListType) nonNullType.getType();
                    if (_listType.getType() instanceof NonNullType) {
                        isListNonEmpty = true;
                        NonNullType _nonNullType = (NonNullType) _listType.getType();

                        inputTypeName = ((TypeName) _nonNullType.getType()).getName();
                    } else {
                        inputTypeName = ((TypeName) _listType.getType()).getName();
                    }
                } else {
                    inputTypeName = ((TypeName) nonNullType.getType()).getName();
                }
            } else if (fieldType instanceof ListType) {
                isList = true;
                ListType listType = (ListType) fieldType;
                if (listType.getType() instanceof NonNullType) {
                    isListNonEmpty = true;
                    NonNullType nonNullType = (NonNullType) listType.getType();
                    inputTypeName = ((TypeName) nonNullType.getType()).getName();
                } else if (listType.getType() instanceof ListType) {

                    // retailerRefs: [[String]]
                    ListType _listType = (ListType) listType.getType();
                    inputTypeName = ((TypeName) _listType.getType()).getName();
                } else {
                    inputTypeName = ((TypeName) listType.getType()).getName();
                }

            } else {
                inputTypeName = ((TypeName) inputValueDefinition.getType()).getName();
            }

            com.squareup.javapoet.TypeName fieldTypeName = null;
            if (inputTypeName.equals("ID") || inputTypeName.equals("String") || inputTypeName.equals("Json")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(String.class);
            } else if (inputTypeName.equals("DateTime")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Date.class);
            } else if (inputTypeName.equals("Boolean")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Boolean.class);
            } else if (inputTypeName.equals("Int")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Integer.class);
            } else if (inputTypeName.equals(("Float"))) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Float.class);
            } else {
                fieldTypeName = ClassName.get(modulePackageName, inputTypeName);
                log.info("Custom type [{} {}] defined", inputTypeName, fieldTypeName);
            }

            if (isList) {
                ClassName list = ClassName.get("java.util", "List");
                fieldTypeName = ParameterizedTypeName.get(list, fieldTypeName);
            }

            FieldSpec.Builder fieldBuilder = FieldSpec.builder(fieldTypeName, inputValueDefinition.getName(), Modifier.PRIVATE);

            String typeDefinitionStr = inputTypeName;
            if (isNonNull) {
                fieldBuilder.addAnnotation(Nonnull.class);
                typeDefinitionStr += "!";
            }

            if (isListNonEmpty) {
                fieldBuilder.addAnnotation(NotEmpty.class);
                typeDefinitionStr = "["+typeDefinitionStr+"]";
            }

            fieldBuilder.addAnnotation(getFieldSpec(inputValueDefinition, typeDefinitionStr));

            inputValueDefinition.getDirectives().forEach(directive -> {

                if (DIRECTIVE_CACHE_KEY.equals(directive.getName())) {
                    Argument matchFieldArg = directive.getArgument("matchedField");
                    Argument groupsArg = directive.getArgument("group");

                    if (matchFieldArg == null || groupsArg == null) {
                        throw new CodeGenException(DIRECTIVE_CACHE_KEY + " missing mandatory arguments in field \"" + inputValueDefinition.getName() + "\"");
                    }

                    CodeBlock.Builder groups = CodeBlock.builder();

                    ArrayValue groupVals = (ArrayValue) groupsArg.getValue();
                    groupVals.getValues().forEach(groupVal -> {

                        if (groups.isEmpty()) {
                            groups.add("$S", ((StringValue) groupVal).getValue());
                        } else {
                            groups.add(", $S", ((StringValue) groupVal).getValue());
                        }
                    });

                    AnnotationSpec.Builder cacheKeyAnnotationBuilder = AnnotationSpec
                            .builder(CacheKey.class)
                            .addMember("matchedField", "$S", ((StringValue) matchFieldArg.getValue()).getValue());

                    if (groupVals.getValues().size() > 1) {
                        cacheKeyAnnotationBuilder.addMember("group", "{$L}", groups.build());
                    } else {
                        cacheKeyAnnotationBuilder.addMember("group", groups.build());
                    }

                    fieldBuilder.addAnnotation(cacheKeyAnnotationBuilder.build());

                }
            });

            builder.addField(fieldBuilder.build());
        });

        ClassName queryResponse = ClassName.get(modulePackageName, output.getName());

        ClassName _queryClazz = ClassName.get(artemis.graphql.Query.class.getPackage().getName(), artemis.graphql.Query.class.getSimpleName());
        ParameterizedTypeName queryClazz = ParameterizedTypeName.get(_queryClazz, queryResponse);

        builder.addSuperinterface(queryClazz);
        builder.addAnnotation(AnnotationSpec.builder(Operational.class).addMember("name", "$S", field.getName())
                .addMember("type", "$T.$L", OperationType.class, OperationType.QUERY.name()).build());


        ClassName _classType = ClassName.get("java.lang", "Class");
        ParameterizedTypeName classType = ParameterizedTypeName.get(_classType, queryResponse);

        builder.addMethod(MethodSpec.methodBuilder("responseType").addModifiers(Modifier.PUBLIC, Modifier.FINAL).addAnnotation(Override.class).returns(classType).addStatement("return $T.class", queryResponse).build());

        JavaFile javaFile = JavaFile.builder(queryPackageName, builder.build()).build();

        createSourceCode(GEN_SOURCE, javaFile);
    }


    public void generateMutation(String modulePackageName, String mutationPackageName, FieldDefinition field, TypeDefinition output) throws IOException {
        String className = CodeGenUtil.camelCase(field.getName() + "Mutation");
        TypeSpec.Builder builder = TypeSpec.classBuilder(className)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(getGeneratedSpec())
                .addAnnotation(lombok.Value.class)
                .addAnnotation(AnnotationSpec.builder(Builder.class)
                        .addMember("builderClassName", "$S", "Builder")
                        .addMember("toBuilder", "$L", true).build())
                .addAnnotation(AnnotationSpec.builder(JsonDeserialize.class)
                        .addMember("builder", "$T.class", ClassName.get(mutationPackageName, className + ".Builder")).build()
                );


        field.getInputValueDefinitions().forEach(inputValueDefinition -> {
            graphql.language.Type fieldType = inputValueDefinition.getType();
            boolean isList = false;
            boolean isNonNull = false;
            boolean isListNonEmpty = false;

            String inputTypeName = null;

            if (fieldType instanceof NonNullType) {
                NonNullType nonNullType = (NonNullType) fieldType;
                isNonNull = true;
                log.info("{}.{} type = {}, is ListType? {}, isNonnull type? {}", field.getName(), inputValueDefinition.getName(), inputValueDefinition.getType(), nonNullType.getType() instanceof ListType, nonNullType.getType() instanceof TypeName);

                if (nonNullType.getType() instanceof ListType) {
                    isList = true;
                    ListType _listType = (ListType) nonNullType.getType();
                    if (_listType.getType() instanceof NonNullType) {
                        isListNonEmpty = true;
                        NonNullType _nonNullType = (NonNullType) _listType.getType();

                        inputTypeName = ((TypeName) _nonNullType.getType()).getName();
                    } else {
                        inputTypeName = ((TypeName) _listType.getType()).getName();
                    }
                } else {
                    inputTypeName = ((TypeName) nonNullType.getType()).getName();
                }
            } else if (fieldType instanceof ListType) {
                isList = true;
                ListType listType = (ListType) fieldType;
                if (listType.getType() instanceof NonNullType) {
                    isListNonEmpty = true;
                    NonNullType nonNullType = (NonNullType) listType.getType();
                    inputTypeName = ((TypeName) nonNullType.getType()).getName();
                } else if (listType.getType() instanceof ListType) {

                    // retailerRefs: [[String]]
                    ListType _listType = (ListType) listType.getType();
                    inputTypeName = ((TypeName) _listType.getType()).getName();
                } else {
                    inputTypeName = ((TypeName) listType.getType()).getName();
                }

            } else {
                inputTypeName = ((TypeName) inputValueDefinition.getType()).getName();
            }

            com.squareup.javapoet.TypeName fieldTypeName = null;
            if (inputTypeName.equals("ID") || inputTypeName.equals("String") || inputTypeName.equals("Json")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(String.class);
            } else if (inputTypeName.equals("DateTime")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Date.class);
            } else if (inputTypeName.equals("Boolean")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Boolean.class);
            } else if (inputTypeName.equals("Int")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Integer.class);
            } else if (inputTypeName.equals(("Float"))) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Float.class);
            } else {
                fieldTypeName = ClassName.get(modulePackageName, inputTypeName);
                log.info("Custom type [{} {}] defined", inputTypeName, fieldTypeName);
            }


            if (isList) {
                ClassName list = ClassName.get("java.util", "List");
                fieldTypeName = ParameterizedTypeName.get(list, fieldTypeName);
            }

            FieldSpec.Builder fieldBuilder = FieldSpec.builder(fieldTypeName, inputValueDefinition.getName(), Modifier.PRIVATE);

            if (isNonNull) {
                fieldBuilder.addAnnotation(Nonnull.class);
            }

            if (isListNonEmpty) {
                fieldBuilder.addAnnotation(NotEmpty.class);
            }

            builder.addField(fieldBuilder.build());
        });

        ClassName mutationResponse = ClassName.get(modulePackageName, output.getName());

        ClassName _mutationClazz = ClassName.get(artemis.graphql.Mutation.class.getPackage().getName(), artemis.graphql.Mutation.class.getSimpleName());
        ParameterizedTypeName mutationClazz = ParameterizedTypeName.get(_mutationClazz, mutationResponse);

        builder.addSuperinterface(mutationClazz);
        builder.addAnnotation(AnnotationSpec.builder(Operational.class).addMember("name", "$S", field.getName())
                .addMember("type", "$T.$L", OperationType.class, OperationType.QUERY.name()).build());


        ClassName _classType = ClassName.get("java.lang", "Class");
        ParameterizedTypeName classType = ParameterizedTypeName.get(_classType, mutationResponse);

        builder.addMethod(MethodSpec.methodBuilder("responseType").addModifiers(Modifier.PUBLIC, Modifier.FINAL).addAnnotation(Override.class).returns(classType).addStatement("return $T.class", mutationResponse).build());

        JavaFile javaFile = JavaFile.builder(mutationPackageName, builder.build()).build();

        createSourceCode(GEN_SOURCE, javaFile);
    }

    public void generateInterface(String packageName, InterfaceTypeDefinition typeDefinition) throws IOException {
        TypeSpec.Builder builder = TypeSpec
                .interfaceBuilder(typeDefinition.getName())
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(getGeneratedSpec());

        typeDefinition.getFieldDefinitions().stream().forEach(definition -> {
            graphql.language.Type fieldType = definition.getType();

            log.info("Generating Interface[{}] {}", typeDefinition.getName(), fieldType);

            MethodSpec.Builder methodBuilder = MethodSpec
                    .methodBuilder("get" + CodeGenUtil.camelCase(definition.getName()));


            String typeName = null;
            boolean isList = false;
            if (fieldType instanceof NonNullType) {
                NonNullType nonNullType = (NonNullType) fieldType;
                methodBuilder.addAnnotation(Nonnull.class);
                typeName = ((TypeName) nonNullType.getType()).getName();
            } else if (fieldType instanceof ListType) {
                isList = true;
                ListType listType = (ListType) fieldType;
                typeName = ((TypeName) listType.getType()).getName();
            } else {
                typeName = ((TypeName) definition.getType()).getName();
            }

            com.squareup.javapoet.TypeName returnTypeName = null;
            if (typeName.equals("ID") || typeName.equals("String") || typeName.equals("Json")) {
                returnTypeName = com.squareup.javapoet.TypeName.get(String.class);
            } else if (typeName.equals("DateTime")) {
                returnTypeName = com.squareup.javapoet.TypeName.get(Date.class);
            } else if (typeName.equals("Boolean")) {
                returnTypeName = com.squareup.javapoet.TypeName.get(Boolean.class);
            } else if (typeName.equals("Int")) {
                returnTypeName = com.squareup.javapoet.TypeName.get(Integer.class);
            } else if (typeName.equals(("Float"))) {
                returnTypeName = com.squareup.javapoet.TypeName.get(Float.class);
            } else {
                returnTypeName = ClassName.get(packageName, typeName);
                log.warn("Custom type [{}] found", typeName);
            }

            if (isList) {
                ClassName list = ClassName.get("java.util", "List");
                returnTypeName = ParameterizedTypeName.get(list, returnTypeName);
            }

            methodBuilder.addAnnotation(getFieldSpec(definition, typeName)).returns(returnTypeName);

            builder.addMethod(methodBuilder.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT).build());

        });

        JavaFile javaFile = JavaFile.builder(packageName, builder.build()).build();
        createSourceCode(GEN_SOURCE, javaFile);
    }

    public void generateInputObject(String packageName, InputObjectTypeDefinition typeDefinition) throws IOException {
        TypeSpec.Builder builder = TypeSpec.classBuilder(typeDefinition.getName())
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(getGeneratedSpec())
                .addAnnotation(lombok.Value.class)
                .addAnnotation(AnnotationSpec.builder(Builder.class)
                        .addMember("builderClassName", "$S", "Builder")
                        .addMember("toBuilder", "$L", true).build())
                .addAnnotation(AnnotationSpec.builder(JsonDeserialize.class)
                        .addMember("builder", "$T.class", ClassName.get(packageName, typeDefinition.getName() + ".Builder")).build()
                );

        typeDefinition.getInputValueDefinitions().stream().forEach(definition -> {
            graphql.language.Type fieldType = definition.getType();
            log.info("Generating Class[{}] {}", typeDefinition.getName(), fieldType);

            boolean isList = false;
            boolean isNonNull = false;
            boolean isListNonEmpty = false;

            String typeName = null;

            if (fieldType instanceof NonNullType) {
                NonNullType nonNullType = (NonNullType) fieldType;
                isNonNull = true;
                log.info("{}.{} type = {}, is ListType? {}, isNonnull type? {}", typeDefinition.getName(), definition.getName(), definition.getType(), nonNullType.getType() instanceof ListType, nonNullType.getType() instanceof TypeName);

                if (nonNullType.getType() instanceof ListType) {
                    isList = true;
                    ListType _listType = (ListType) nonNullType.getType();
                    log.info(">> {} ", _listType.getType());
                    if (_listType.getType() instanceof NonNullType) {
                        isListNonEmpty = true;
                        NonNullType _nonNullType = (NonNullType) _listType.getType();

                        typeName = ((TypeName) _nonNullType.getType()).getName();
                    } else {
                        typeName = ((TypeName) _listType.getType()).getName();
                    }
                } else {
                    typeName = ((TypeName) nonNullType.getType()).getName();
                }
            } else if (fieldType instanceof ListType) {
                isList = true;
                ListType listType = (ListType) fieldType;
                if (listType.getType() instanceof NonNullType) {
                    isListNonEmpty = true;
                    NonNullType nonNullType = (NonNullType) listType.getType();
                    typeName = ((TypeName) nonNullType.getType()).getName();
                } else if (listType.getType() instanceof ListType) {

                    // retailerRefs: [[String]]
                    ListType _listType = (ListType) listType.getType();
                    typeName = ((TypeName) _listType.getType()).getName();
                } else {
                    typeName = ((TypeName) listType.getType()).getName();
                }

            } else {
                typeName = ((TypeName) definition.getType()).getName();
            }

            com.squareup.javapoet.TypeName fieldTypeName = null;
            if (typeName.equals("ID") || typeName.equals("String") || typeName.equals("Json")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(String.class);
            } else if (typeName.equals("DateTime")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Date.class);
            } else if (typeName.equals("Boolean")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Boolean.class);
            } else if (typeName.equals("Int")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Integer.class);
            } else if (typeName.equals(("Float"))) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Float.class);
            } else {
                fieldTypeName = ClassName.get(packageName, typeName);
                log.warn("Custom type [{}] found", typeName);
            }

            if (isList) {
                ClassName list = ClassName.get("java.util", "List");
                fieldTypeName = ParameterizedTypeName.get(list, fieldTypeName);
            }

            FieldSpec.Builder fieldBuilder = FieldSpec.builder(fieldTypeName, definition.getName(), Modifier.PRIVATE);

            if (isNonNull) {
                fieldBuilder.addAnnotation(Nonnull.class);
            }

            if (isListNonEmpty) {
                fieldBuilder.addAnnotation(NotEmpty.class);
            }
            builder.addField(fieldBuilder.build()).build();

        });

        JavaFile javaFile = JavaFile.builder(packageName, builder.build()).build();

        createSourceCode(GEN_SOURCE, javaFile);
    }

    public void generateEnum(String packageName, EnumTypeDefinition typeDefinition) throws IOException {
        TypeSpec.Builder builder = TypeSpec.enumBuilder(typeDefinition.getName())
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(getGeneratedSpec());

        typeDefinition.getEnumValueDefinitions().stream()
                .forEach(enumDefinition -> builder.addEnumConstant(enumDefinition.getName()));

        JavaFile javaFile = JavaFile.builder(packageName, builder.build()).build();

        createSourceCode(GEN_SOURCE, javaFile);
    }

    public void generateObject(String packageName, ObjectTypeDefinition typeDefinition) throws IOException {
        TypeSpec.Builder builder = TypeSpec.classBuilder(typeDefinition.getName())
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(getGeneratedSpec())
                .addAnnotation(lombok.Value.class)
                .addAnnotation(AnnotationSpec.builder(Builder.class)
                        .addMember("builderClassName", "$S", "Builder")
                        .addMember("toBuilder", "$L", true).build())
                .addAnnotation(AnnotationSpec.builder(JsonDeserialize.class)
                        .addMember("builder", "$T.class", ClassName.get(packageName, typeDefinition.getName() + ".Builder")).build()
                );

        // Add interface
        typeDefinition.getImplements().stream().forEach(type -> {
            com.squareup.javapoet.TypeName interfaceType = ClassName.get(packageName, ((TypeName) type).getName());
            builder.addSuperinterface(interfaceType);
        });

        typeDefinition.getFieldDefinitions().stream().forEach(definition -> {
            graphql.language.Type fieldType = definition.getType();
            log.info("Generating Class[{}] {}", typeDefinition.getName(), fieldType);

            String typeName = null;
            boolean isList = false;
            boolean isNonnull = false;
            if (fieldType instanceof NonNullType) {
                isNonnull = true;
                NonNullType nonNullType = (NonNullType) fieldType;
                if (nonNullType.getType() instanceof ListType) {
                    typeName = ((TypeName) ((ListType) nonNullType.getType()).getType()).getName();
                } else {
                    typeName = ((TypeName) nonNullType.getType()).getName();
                }
            } else if (fieldType instanceof ListType) {
                isList = true;
                ListType listType = (ListType) fieldType;
                typeName = ((TypeName) listType.getType()).getName();
            } else {
                typeName = ((TypeName) definition.getType()).getName();
            }

            com.squareup.javapoet.TypeName fieldTypeName = null;
            if (typeName.equals("ID") || typeName.equals("String") || typeName.equals("Json")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(String.class);
            } else if (typeName.equals("DateTime")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Date.class);
            } else if (typeName.equals("Boolean")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Boolean.class);
            } else if (typeName.equals("Int")) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Integer.class);
            } else if (typeName.equals(("Float"))) {
                fieldTypeName = com.squareup.javapoet.TypeName.get(Float.class);
            } else {
                fieldTypeName = ClassName.get(packageName, typeName);
                log.warn("Custom type [{}] found", typeName);
            }


            if (isList) {
                ClassName list = ClassName.get("java.util", "List");
                fieldTypeName = ParameterizedTypeName.get(list, fieldTypeName);
            }

            FieldSpec.Builder fieldBuilder = FieldSpec
                    .builder(fieldTypeName, definition.getName(), Modifier.PRIVATE);

            if (isNonnull) {
                fieldBuilder.addAnnotation(Nonnull.class);
            }
            fieldBuilder.addAnnotation(getFieldSpec(definition, typeName));

            builder.addField(fieldBuilder.build()).build();

        });

        builder.addType(TypeSpec.classBuilder("Builder")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                .addAnnotation(AnnotationSpec.builder(JsonPOJOBuilder.class).addMember("withPrefix", "$S", "").build())
                .build());

        JavaFile javaFile = JavaFile.builder(packageName, builder.build()).build();

        createSourceCode(GEN_SOURCE, javaFile);
    }

    public static void createSourceCode(String path, JavaFile javaFile) throws IOException {
        File file = new File(path);
        javaFile.writeTo(file);

    }

    private AnnotationSpec getGeneratedSpec() {
        return AnnotationSpec.builder(Generated.class)
                .addMember("date", "$S", ZonedDateTime.now().truncatedTo(SECONDS).format(ISO_OFFSET_DATE_TIME))
                .addMember("value", "$S", CodeGenerator.class.getName())
                .addMember("comments", "$S", "Auto Generated by Artemis generator. DO NOT EDIT.")
                .build();
    }

    private AnnotationSpec getFieldSpec(FieldDefinition definition, String typeName) {
        return AnnotationSpec.builder(Field.class)
                .addMember("name", "$S", definition.getName())
                .addMember("typeDefinition", "$S", typeName)
                .build();
    }

    private AnnotationSpec getFieldSpec(InputValueDefinition definition, String typeName) {
        return AnnotationSpec
                .builder(InputField.class)
                .addMember("name", "$S", definition.getName())
                .addMember("typeDefinition", "$S", typeName)
                .build();
    }
}
