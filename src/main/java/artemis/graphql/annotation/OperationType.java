package artemis.graphql.annotation;

public enum OperationType {

    /**
     * Query type operation
     */
    QUERY,
    /**
     * Mutation type operation
     */
    MUTATION

}
