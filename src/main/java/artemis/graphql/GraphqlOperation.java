package artemis.graphql;

public interface GraphqlOperation<T> {

    Class<T> responseType();

}
