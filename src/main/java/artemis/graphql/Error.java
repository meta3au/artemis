package artemis.graphql;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
@Builder(
        builderClassName = "Builder",
        toBuilder = true
)
@JsonDeserialize(
        builder = Error.Builder.class
)
public class Error {

    private String message;
    private List<Location> locations;
    private Map<String, Object> customAttributes;

    @JsonPOJOBuilder(
            withPrefix = ""
    )
    public static final class Builder {
    }
}
