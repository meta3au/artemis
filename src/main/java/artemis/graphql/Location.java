package artemis.graphql;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(
        builderClassName = "Builder",
        toBuilder = true
)
@JsonDeserialize(
        builder = Location.Builder.class
)
public class Location {

    private long line;
    private long column;


    @JsonPOJOBuilder(
            withPrefix = ""
    )
    public static final class Builder {
    }
}
