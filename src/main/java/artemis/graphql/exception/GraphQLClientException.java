package artemis.graphql.exception;

import java.util.List;
import artemis.graphql.Error;

public class GraphQLClientException extends RuntimeException {

    private final List<Error> errors;

    public GraphQLClientException(List<Error> errors) {
        this.errors = errors;
    }

    public List<Error> getErrors() {
        return errors;
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder("\n");

        errors.forEach(error -> {
            sb.append(error.getMessage()).append("\n");

            error.getLocations().forEach( location -> {
                sb.append("\t").append(location).append("\n");
            });
            sb.append("\n");
        });

        return sb.toString();
    }
}
