package artemis.graphql.exception;

public class QueryException extends RuntimeException {

    private String operationName;

    private String queryRequest;

    private String queryVariables;

    public QueryException(String operationName, String queryRequest, String queryVariables, Throwable throwable) {
        super(String.format("[%s] Query = %s, Variables = %s", operationName, queryRequest, queryVariables), throwable);
        this.operationName = operationName;
        this.queryRequest = queryRequest;
        this.queryVariables = queryVariables;
    }


    public String getOperationName() {
        return operationName;
    }


    public String getQueryRequest() {
        return queryRequest;
    }


    public String getQueryVariables() {
        return queryVariables;
    }


}