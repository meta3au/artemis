package artemis.graphql.pagination;


import java.util.List;


public interface Connection<N> {

    List<Edge<N>> getEdges();

    PageInfo getPageInfo();
}
