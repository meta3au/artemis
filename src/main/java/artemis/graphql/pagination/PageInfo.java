package artemis.graphql.pagination;


import artemis.graphql.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.lang.Boolean;
import lombok.Builder;
import lombok.Value;


@Value
@Builder(
        builderClassName = "Builder",
        toBuilder = true
)
@JsonDeserialize(
        builder = Builder.class
)
public class PageInfo {
    @Field(
            name = "hasPreviousPage",
            typeDefinition = "Boolean"
    )
    private Boolean hasPreviousPage;

    @Field(
            name = "hasNextPage",
            typeDefinition = "Boolean"
    )
    private Boolean hasNextPage;
}
