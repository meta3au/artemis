package artemis.graphql.pagination;

public interface Edge<N> {

    N getNode();

    String getCursor();
}
