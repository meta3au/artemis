package au.com.meta3.artemis;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LazyLoadHandler implements net.sf.cglib.proxy.InvocationHandler {

    private final Person proxied;
    public static Map<String, String> names = new ConcurrentHashMap<>();

    static {
        names.put("name", "Jeff");
        names.put("age", "38");
    }

    public LazyLoadHandler(Person proxied) {
        this.proxied = proxied;
    }

//
//    @Override
//    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
//        if (method.getReturnType() == List.class) {
//            return Arrays.asList(proxied.getName(), "lazy" + proxied.getName());
//        } else {
//            return methodProxy.invoke(proxied, objects);
//        }
//    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getReturnType() == List.class) {
            return Arrays.asList(proxied.getName(), names.get(proxied.getName()));
        } else {
            return method.invoke(proxied, args);
        }
    }
}


