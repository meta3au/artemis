package au.com.meta3.artemis;

import lombok.Data;
import net.sf.cglib.proxy.Enhancer;

import java.util.Collections;
import java.util.List;

@Data
public class Person {

    String name;


    public List<String> getHelloName() {
        return Collections.emptyList();
    }


    public static void main(String args[]) {
        Person p =new Person();
        p.setName("name");
        Person proxyP =  (Person) Enhancer.create(Person.class, new LazyLoadHandler(p));

        System.out.println(p.getName());
        System.out.println(proxyP.getName());

        System.out.println(p.getHelloName());
        System.out.println(proxyP.getHelloName());

        LazyLoadHandler.names.put("funny", "is really funny");
        p.setName("funny");

        System.out.println(p.getHelloName());
        System.out.println(proxyP.getHelloName());

    }

}
