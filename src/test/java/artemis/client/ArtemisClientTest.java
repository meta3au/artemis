package artemis.client;

import artemis.client.extension.AddressDeserializer;
import artemis.client.impl.DefaultGraphqlDataFetcher;
import com.fluentcommerce.graphql.client.module.*;
import com.fluentcommerce.graphql.client.query.OrderByIdQuery;
import com.fluentcommerce.graphql.client.query.VirtualCatalogueQuery;
import com.fluentcommerce.graphql.client.query.VirtualPositionQuery;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

@Slf4j
public class ArtemisClientTest {

    @Ignore
    @Test
    public void testQuery() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request authRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer 8a73a9c4-62da-4347-8178-0b1c4615f734")
                        .addHeader("fluent.account", "TARGETQAT")
                        .build();
                Response response = chain.proceed(authRequest);

                // re-auth
                if (response.code() == 401) {
                    // re-auth
                }

                return response;
            }
        });

        OkHttpClient httpClient = httpClientBuilder.build();

        ArtemisClientConfig config = ArtemisClientConfig.builder().graphQLEndpoint("https://targetqat.sandbox.api.fluentretail.com/graphql").build();
        ArtemisClient client = ArtemisClient.builder()
                .httpClient(httpClient)
                .config(config)
                .dataFetcher(
                        DefaultGraphqlDataFetcher.builder()
                                .httpClient(httpClient)
                                .config(config)
                                .addDeserializer(Address.class, new AddressDeserializer()).build())
                .build();
//
//        VirtualPositionQuery query = VirtualPositionQuery.builder().ref("HG-base-test-0001").catalogue(VirtualCatalogueKey.builder().ref("BASE:1").build()).build();
//
//        VirtualPosition position = client.query(query);
//
//        log.info("virtualPosition.ref => {}", position.getRef());
//
//        log.info("virtualPosition.catalogue.inventoryCatalogueRef => {}", position.getCatalogue().getInventoryCatalogueRef());
//
//        log.info("virtualPosition.catalogue.id => {}", position.getCatalogue().getId());
//
//
//        VirtualCatalogueQuery catalogueQuery = VirtualCatalogueQuery.builder().ref("BASE:1").build();
//
//        VirtualCatalogue virtualCatalogue = client.query(catalogueQuery);
//
//        log.info("virtualCatalogue.id => {}", virtualCatalogue.getId());
//
//        log.info("virtualCatalogue.retailerRefs => {}", virtualCatalogue.getRetailerRefs());


        Order order = client.query(OrderByIdQuery.builder().id("100").build());

        log.info("order.status => {}", order.getStatus());

        log.info("order.attributes => {}", order.getAttributes().size());
        log.info("order.fulfilmentChoice.deliveryAddress.street => {}", order.getFulfilmentChoice().getDeliveryAddress().getStreet());
        log.info("order.fulfilmentChoice.deliveryAddress.city => {}", order.getFulfilmentChoice().getDeliveryAddress().getCity());
        log.info("order.fulfilmentChoice.deliveryAddress.postcode => {}", order.getFulfilmentChoice().getDeliveryAddress().getPostcode());

        log.info("order.customer.retailer.primaryEmail => {}", order.getCustomer().getRetailer().getPrimaryEmail());
        log.info("order.customer.retailer.websiteUrl => {}", order.getCustomer().getRetailer().getWebsiteUrl());


//
//        InventoryPosition ip = null;
//        ip.getQuantities().getEdges().stream().forEach(e-> {
//            InventoryQuantity quantity = e.getNode();
//            quantity.getCatalogue();
//        });
    }

}