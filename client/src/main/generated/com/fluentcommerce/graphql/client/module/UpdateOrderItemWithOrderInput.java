package com.fluentcommerce.graphql.client.module;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:10+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = UpdateOrderItemWithOrderInput.Builder.class
)
public class UpdateOrderItemWithOrderInput {
  @Nonnull
  private String id;

  private String ref;

  private String productRef;

  private String productCatalogueRef;

  private String status;

  private Integer quantity;

  private Float paidPrice;

  private String currency;

  private Float price;

  private Float taxPrice;

  private String taxType;

  private Float totalPrice;

  private Float totalTaxPrice;
}
