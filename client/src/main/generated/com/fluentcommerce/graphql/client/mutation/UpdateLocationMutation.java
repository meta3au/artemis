package com.fluentcommerce.graphql.client.mutation;

import artemis.graphql.Mutation;
import artemis.graphql.annotation.OperationType;
import artemis.graphql.annotation.Operational;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fluentcommerce.graphql.client.module.Location;
import com.fluentcommerce.graphql.client.module.UpdateLocationInput;
import java.lang.Class;
import java.lang.Override;
import javax.annotation.Generated;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = UpdateLocationMutation.Builder.class
)
@Operational(
    name = "updateLocation",
    type = OperationType.QUERY
)
public class UpdateLocationMutation implements Mutation<Location> {
  private UpdateLocationInput input;

  @Override
  public final Class<Location> responseType() {
    return Location.class;
  }
}
