package com.fluentcommerce.graphql.client.query;

import artemis.graphql.Query;
import artemis.graphql.annotation.InputField;
import artemis.graphql.annotation.OperationType;
import artemis.graphql.annotation.Operational;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fluentcommerce.graphql.client.module.Customer;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = CustomerByIdQuery.Builder.class
)
@Operational(
    name = "customerById",
    type = OperationType.QUERY
)
public class CustomerByIdQuery implements Query<Customer> {
  @Nonnull
  @InputField(
      name = "id",
      typeDefinition = "ID!"
  )
  private String id;

  @Override
  public final Class<Customer> responseType() {
    return Customer.class;
  }
}
