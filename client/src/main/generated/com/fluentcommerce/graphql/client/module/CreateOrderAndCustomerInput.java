package com.fluentcommerce.graphql.client.module;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.lang.Float;
import java.lang.String;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:10+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = CreateOrderAndCustomerInput.Builder.class
)
public class CreateOrderAndCustomerInput {
  @Nonnull
  private String ref;

  @Nonnull
  private String type;

  private List<AttributeInput> attributes;

  @Nonnull
  private RetailerId retailer;

  private Float totalPrice;

  private Float totalTaxPrice;

  private CreateFulfilmentChoiceWithOrderInput fulfilmentChoice;

  @Nonnull
  private CreateCustomerInput customer;

  @Nonnull
  private List<CreateOrderItemWithOrderInput> items;
}
