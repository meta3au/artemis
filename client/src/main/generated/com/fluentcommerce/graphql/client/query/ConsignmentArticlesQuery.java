package com.fluentcommerce.graphql.client.query;

import artemis.graphql.Query;
import artemis.graphql.annotation.InputField;
import artemis.graphql.annotation.OperationType;
import artemis.graphql.annotation.Operational;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fluentcommerce.graphql.client.module.ConsignmentArticleConnection;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import javax.annotation.Generated;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = ConsignmentArticlesQuery.Builder.class
)
@Operational(
    name = "consignmentArticles",
    type = OperationType.QUERY
)
public class ConsignmentArticlesQuery implements Query<ConsignmentArticleConnection> {
  @InputField(
      name = "first",
      typeDefinition = "Int"
  )
  private Integer first;

  @InputField(
      name = "last",
      typeDefinition = "Int"
  )
  private Integer last;

  @InputField(
      name = "before",
      typeDefinition = "String"
  )
  private String before;

  @InputField(
      name = "after",
      typeDefinition = "String"
  )
  private String after;

  @Override
  public final Class<ConsignmentArticleConnection> responseType() {
    return ConsignmentArticleConnection.class;
  }
}
