package com.fluentcommerce.graphql.client.module;

import artemis.graphql.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.lang.String;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = TaxType.Builder.class
)
public class TaxType {
  @Nonnull
  @Field(
      name = "country",
      typeDefinition = "String"
  )
  private String country;

  @Nonnull
  @Field(
      name = "group",
      typeDefinition = "String"
  )
  private String group;

  @Field(
      name = "tariff",
      typeDefinition = "String"
  )
  private String tariff;

  @JsonPOJOBuilder(
      withPrefix = ""
  )
  public static final class Builder {
  }
}
