package com.fluentcommerce.graphql.client.query;

import artemis.graphql.Query;
import artemis.graphql.annotation.InputField;
import artemis.graphql.annotation.OperationType;
import artemis.graphql.annotation.Operational;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fluentcommerce.graphql.client.module.DateRange;
import com.fluentcommerce.graphql.client.module.ProductCatalogueConnection;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import javax.annotation.Generated;
import javax.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = ProductCataloguesQuery.Builder.class
)
@Operational(
    name = "productCatalogues",
    type = OperationType.QUERY
)
public class ProductCataloguesQuery implements Query<ProductCatalogueConnection> {
  @InputField(
      name = "createdOn",
      typeDefinition = "DateRange"
  )
  private DateRange createdOn;

  @InputField(
      name = "updatedOn",
      typeDefinition = "DateRange"
  )
  private DateRange updatedOn;

  @NotEmpty
  @InputField(
      name = "ref",
      typeDefinition = "[String]"
  )
  private List<String> ref;

  @NotEmpty
  @InputField(
      name = "type",
      typeDefinition = "[String]"
  )
  private List<String> type;

  @NotEmpty
  @InputField(
      name = "workflowRef",
      typeDefinition = "[String]"
  )
  private List<String> workflowRef;

  @NotEmpty
  @InputField(
      name = "workflowVersion",
      typeDefinition = "[Int]"
  )
  private List<Integer> workflowVersion;

  @InputField(
      name = "status",
      typeDefinition = "String"
  )
  private List<String> status;

  @NotEmpty
  @InputField(
      name = "name",
      typeDefinition = "[String]"
  )
  private List<String> name;

  @InputField(
      name = "description",
      typeDefinition = "String"
  )
  private List<String> description;

  @InputField(
      name = "retailerRefs",
      typeDefinition = "String"
  )
  private List<String> retailerRefs;

  @InputField(
      name = "first",
      typeDefinition = "Int"
  )
  private Integer first;

  @InputField(
      name = "last",
      typeDefinition = "Int"
  )
  private Integer last;

  @InputField(
      name = "before",
      typeDefinition = "String"
  )
  private String before;

  @InputField(
      name = "after",
      typeDefinition = "String"
  )
  private String after;

  @Override
  public final Class<ProductCatalogueConnection> responseType() {
    return ProductCatalogueConnection.class;
  }
}
