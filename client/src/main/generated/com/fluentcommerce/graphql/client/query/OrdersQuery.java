package com.fluentcommerce.graphql.client.query;

import artemis.graphql.Query;
import artemis.graphql.annotation.InputField;
import artemis.graphql.annotation.OperationType;
import artemis.graphql.annotation.Operational;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fluentcommerce.graphql.client.module.DateRange;
import com.fluentcommerce.graphql.client.module.OrderConnection;
import java.lang.Class;
import java.lang.Float;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import javax.annotation.Generated;
import javax.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = OrdersQuery.Builder.class
)
@Operational(
    name = "orders",
    type = OperationType.QUERY
)
public class OrdersQuery implements Query<OrderConnection> {
  @InputField(
      name = "ref",
      typeDefinition = "String"
  )
  private List<String> ref;

  @NotEmpty
  @InputField(
      name = "type",
      typeDefinition = "[String]"
  )
  private List<String> type;

  @InputField(
      name = "status",
      typeDefinition = "String"
  )
  private List<String> status;

  @NotEmpty
  @InputField(
      name = "workflowRef",
      typeDefinition = "[String]"
  )
  private List<String> workflowRef;

  @NotEmpty
  @InputField(
      name = "workflowVersion",
      typeDefinition = "[Int]"
  )
  private List<Integer> workflowVersion;

  @InputField(
      name = "createdOn",
      typeDefinition = "DateRange"
  )
  private DateRange createdOn;

  @InputField(
      name = "updatedOn",
      typeDefinition = "DateRange"
  )
  private DateRange updatedOn;

  @InputField(
      name = "totalPrice",
      typeDefinition = "Float"
  )
  private List<Float> totalPrice;

  @InputField(
      name = "totalTaxPrice",
      typeDefinition = "Float"
  )
  private List<Float> totalTaxPrice;

  @InputField(
      name = "first",
      typeDefinition = "Int"
  )
  private Integer first;

  @InputField(
      name = "last",
      typeDefinition = "Int"
  )
  private Integer last;

  @InputField(
      name = "before",
      typeDefinition = "String"
  )
  private String before;

  @InputField(
      name = "after",
      typeDefinition = "String"
  )
  private String after;

  @Override
  public final Class<OrderConnection> responseType() {
    return OrderConnection.class;
  }
}
