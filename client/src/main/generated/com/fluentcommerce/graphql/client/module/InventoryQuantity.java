package com.fluentcommerce.graphql.client.module;

import artemis.graphql.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = InventoryQuantity.Builder.class
)
public class InventoryQuantity implements Node, Referenceable, Orchestrateable, Extendable {
  @Nonnull
  @Field(
      name = "id",
      typeDefinition = "ID"
  )
  private String id;

  @Field(
      name = "createdOn",
      typeDefinition = "DateTime"
  )
  private Date createdOn;

  @Field(
      name = "updatedOn",
      typeDefinition = "DateTime"
  )
  private Date updatedOn;

  @Nonnull
  @Field(
      name = "ref",
      typeDefinition = "String"
  )
  private String ref;

  @Nonnull
  @Field(
      name = "type",
      typeDefinition = "String"
  )
  private String type;

  @Nonnull
  @Field(
      name = "workflowRef",
      typeDefinition = "String"
  )
  private String workflowRef;

  @Nonnull
  @Field(
      name = "workflowVersion",
      typeDefinition = "Int"
  )
  private Integer workflowVersion;

  @Field(
      name = "status",
      typeDefinition = "String"
  )
  private String status;

  @Field(
      name = "attributes",
      typeDefinition = "Attribute"
  )
  private List<Attribute> attributes;

  @Field(
      name = "quantity",
      typeDefinition = "Int"
  )
  private Integer quantity;

  @Field(
      name = "condition",
      typeDefinition = "String"
  )
  private String condition;

  @Field(
      name = "expectedOn",
      typeDefinition = "DateTime"
  )
  private Date expectedOn;

  @Field(
      name = "storageAreaRef",
      typeDefinition = "String"
  )
  private String storageAreaRef;

  @Field(
      name = "catalogue",
      typeDefinition = "InventoryCatalogue"
  )
  private InventoryCatalogue catalogue;

  @Field(
      name = "position",
      typeDefinition = "InventoryPosition"
  )
  private InventoryPosition position;

  @JsonPOJOBuilder(
      withPrefix = ""
  )
  public static final class Builder {
  }
}
