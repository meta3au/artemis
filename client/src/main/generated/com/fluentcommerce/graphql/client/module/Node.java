package com.fluentcommerce.graphql.client.module;

import artemis.graphql.annotation.Field;
import java.lang.String;
import java.util.Date;
import javax.annotation.Generated;
import javax.annotation.Nonnull;

@Generated(
    date = "2018-09-24T00:45:10+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
public interface Node {
  @Nonnull
  @Field(
      name = "id",
      typeDefinition = "ID"
  )
  String getId();

  @Field(
      name = "createdOn",
      typeDefinition = "DateTime"
  )
  Date getCreatedOn();

  @Field(
      name = "updatedOn",
      typeDefinition = "DateTime"
  )
  Date getUpdatedOn();
}
