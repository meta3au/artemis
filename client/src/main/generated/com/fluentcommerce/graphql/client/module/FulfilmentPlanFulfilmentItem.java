package com.fluentcommerce.graphql.client.module;

import artemis.graphql.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.lang.Integer;
import java.lang.String;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = FulfilmentPlanFulfilmentItem.Builder.class
)
public class FulfilmentPlanFulfilmentItem {
  @Nonnull
  @Field(
      name = "productRef",
      typeDefinition = "String"
  )
  private String productRef;

  @Field(
      name = "catalogueRef",
      typeDefinition = "String"
  )
  private String catalogueRef;

  @Nonnull
  @Field(
      name = "availableQuantity",
      typeDefinition = "Int"
  )
  private Integer availableQuantity;

  @Nonnull
  @Field(
      name = "requestedQuantity",
      typeDefinition = "Int"
  )
  private Integer requestedQuantity;

  @JsonPOJOBuilder(
      withPrefix = ""
  )
  public static final class Builder {
  }
}
