package com.fluentcommerce.graphql.client.module;

import artemis.graphql.annotation.Field;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.lang.Boolean;
import java.lang.String;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = User.Builder.class
)
public class User implements Node {
  @Nonnull
  @Field(
      name = "id",
      typeDefinition = "ID"
  )
  private String id;

  @Nonnull
  @Field(
      name = "ref",
      typeDefinition = "String"
  )
  private String ref;

  @Nonnull
  @Field(
      name = "username",
      typeDefinition = "String"
  )
  private String username;

  @Field(
      name = "title",
      typeDefinition = "String"
  )
  private String title;

  @Field(
      name = "firstName",
      typeDefinition = "String"
  )
  private String firstName;

  @Field(
      name = "lastName",
      typeDefinition = "String"
  )
  private String lastName;

  @Field(
      name = "primaryEmail",
      typeDefinition = "String"
  )
  private String primaryEmail;

  @Field(
      name = "primaryPhone",
      typeDefinition = "String"
  )
  private String primaryPhone;

  @Nonnull
  @Field(
      name = "type",
      typeDefinition = "String"
  )
  private String type;

  @Field(
      name = "status",
      typeDefinition = "String"
  )
  private String status;

  @Field(
      name = "attributes",
      typeDefinition = "Attribute"
  )
  private List<Attribute> attributes;

  @Field(
      name = "department",
      typeDefinition = "String"
  )
  private String department;

  @Field(
      name = "country",
      typeDefinition = "String"
  )
  private String country;

  @Field(
      name = "timezone",
      typeDefinition = "String"
  )
  private String timezone;

  @Field(
      name = "promotionOptIn",
      typeDefinition = "Boolean"
  )
  private Boolean promotionOptIn;

  @Field(
      name = "primaryRetailer",
      typeDefinition = "Retailer"
  )
  private Retailer primaryRetailer;

  @Field(
      name = "primaryLocation",
      typeDefinition = "Location"
  )
  private Location primaryLocation;

  @Field(
      name = "createdOn",
      typeDefinition = "DateTime"
  )
  private Date createdOn;

  @Field(
      name = "updatedOn",
      typeDefinition = "DateTime"
  )
  private Date updatedOn;

  @JsonPOJOBuilder(
      withPrefix = ""
  )
  public static final class Builder {
  }
}
