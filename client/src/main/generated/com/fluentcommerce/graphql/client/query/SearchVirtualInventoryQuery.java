package com.fluentcommerce.graphql.client.query;

import artemis.graphql.Query;
import artemis.graphql.annotation.InputField;
import artemis.graphql.annotation.OperationType;
import artemis.graphql.annotation.Operational;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fluentcommerce.graphql.client.module.GeoCoordinateInput;
import com.fluentcommerce.graphql.client.module.ProductQuantityInput;
import com.fluentcommerce.graphql.client.module.VirtualCatalogueKey;
import com.fluentcommerce.graphql.client.module.VirtualInventoryConnection;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = SearchVirtualInventoryQuery.Builder.class
)
@Operational(
    name = "searchVirtualInventory",
    type = OperationType.QUERY
)
public class SearchVirtualInventoryQuery implements Query<VirtualInventoryConnection> {
  @Nonnull
  @InputField(
      name = "virtualCatalogue",
      typeDefinition = "VirtualCatalogueKey!"
  )
  private VirtualCatalogueKey virtualCatalogue;

  @Nonnull
  @NotEmpty
  @InputField(
      name = "productQuantities",
      typeDefinition = "[ProductQuantityInput!]"
  )
  private List<ProductQuantityInput> productQuantities;

  @NotEmpty
  @InputField(
      name = "excludedLocationRefs",
      typeDefinition = "[String]"
  )
  private List<String> excludedLocationRefs;

  @InputField(
      name = "orderByProximity",
      typeDefinition = "GeoCoordinateInput"
  )
  private GeoCoordinateInput orderByProximity;

  @InputField(
      name = "first",
      typeDefinition = "Int"
  )
  private Integer first;

  @Override
  public final Class<VirtualInventoryConnection> responseType() {
    return VirtualInventoryConnection.class;
  }
}
