package com.fluentcommerce.graphql.client.module;

import javax.annotation.Generated;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
public enum ExecutionMode {
  AWAIT_ORCHESTRATION
}
