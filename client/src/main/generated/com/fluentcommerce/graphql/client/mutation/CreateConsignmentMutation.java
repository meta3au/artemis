package com.fluentcommerce.graphql.client.mutation;

import artemis.graphql.Mutation;
import artemis.graphql.annotation.OperationType;
import artemis.graphql.annotation.Operational;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fluentcommerce.graphql.client.module.Consignment;
import com.fluentcommerce.graphql.client.module.CreateConsignmentInput;
import java.lang.Class;
import java.lang.Override;
import javax.annotation.Generated;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = CreateConsignmentMutation.Builder.class
)
@Operational(
    name = "createConsignment",
    type = OperationType.QUERY
)
public class CreateConsignmentMutation implements Mutation<Consignment> {
  private CreateConsignmentInput input;

  @Override
  public final Class<Consignment> responseType() {
    return Consignment.class;
  }
}
