package com.fluentcommerce.graphql.client.mutation;

import artemis.graphql.Mutation;
import artemis.graphql.annotation.OperationType;
import artemis.graphql.annotation.Operational;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fluentcommerce.graphql.client.module.CreateStandardProductInput;
import com.fluentcommerce.graphql.client.module.StandardProduct;
import java.lang.Class;
import java.lang.Override;
import javax.annotation.Generated;
import lombok.Builder;
import lombok.Value;

@Generated(
    date = "2018-09-24T00:45:11+10:00",
    value = "artemis.generator.CodeGenerator",
    comments = "Auto Generated by Artemis generator. DO NOT EDIT."
)
@Value
@Builder(
    builderClassName = "Builder",
    toBuilder = true
)
@JsonDeserialize(
    builder = CreateStandardProductMutation.Builder.class
)
@Operational(
    name = "createStandardProduct",
    type = OperationType.QUERY
)
public class CreateStandardProductMutation implements Mutation<StandardProduct> {
  private CreateStandardProductInput input;

  @Override
  public final Class<StandardProduct> responseType() {
    return StandardProduct.class;
  }
}
